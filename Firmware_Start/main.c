/*
 * Firmware_TTL.c
 *
 * Created: 6/09/2016 7:20:24 PM
 * Author : ENGG2800 Project (Team 17)
 */

#define F_CPU 8000000 // Clock Speed
#define BAUD 9600 // might want to increase this to send data quicker
#define UBRR_VALUE ((F_CPU/16/BAUD)-1)

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <util/delay.h>
#include <time.h>
#include <string.h>
#include "timer0.h"

short recording, saved;
uint32_t previousTicks;
volatile Data* data;

void USART0SendByte(char cdata)
{
	//wait while previous byte is completed
	while(!(UCSR0A &(1<<UDRE0))){};
	// Transmit data
	UDR0 = cdata;
}

void USART0Init(void)
{
	// Set baud rate
	UBRR0H = (uint8_t)(UBRR_VALUE>>8);
	UBRR0L = (uint8_t)UBRR_VALUE;
	// Set frame format to 8 data bits, no parity, 1 stop bit
	UCSR0C |= (1<<UCSZ01)|(1<<UCSZ00);
	//enable transmission and reception
	UCSR0B |= (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
	
	DDRC = 0x38; // makes 5,4,3 outputs and 0,1,2 inputs
	DDRD |= (1 << 1);
	DDRD &= ~1;
}

char USART0ReceiveByte()
{
	// Wait for byte to be received
	//loop_until_bit_is_set(UCSR0A, RXC0);
	while(!(UCSR0A&(1<<RXC0))){};
	// Return received data
	return UDR0;
}

uint16_t GetADCValue(uint8_t channel) {
	ADMUX = ((ADMUX & 0xF0) | channel);
	ADMUX |= ((1<<REFS0) | (1<<REFS1));
	ADCSRA |= (1 << ADSC);
	while (ADCSRA & (1 << ADSC));
	return ADC;
}

void nmea_parse(char *start, int skips, char* result) {
	char* ptr = start;
	for(int i = 0; i < skips; i++) {
		ptr += strcspn(ptr, ",") + 1;
	}
	strncpy(result, ptr, strcspn(ptr, ","));
	result[strcspn(ptr, ",")] = '\0';
}

void receive_gps() {
	int i = 0;
	char *received = malloc(102);
	//receiving from GPS module.
	while((received[i++] = USART0ReceiveByte()) != '\n') {
		//reallocating if NMEA sentences are too long for received buffer.
		if(!(i % 50) && i >= 100) {
			realloc(received, i + 51);
		}
	}
	received[i] = '\0';
	nmea_parse(received, 9, data->date);
	nmea_parse(received, 1, data->time);
	for(int i = 0; i < 2; i++) {
		nmea_parse(received, 3+2*i, data->coords[i].coord);
		nmea_parse(received, 4+2*i, data->coords[i].bearing);	
	}
	free(received);
}

void EEPROM_write(unsigned int uiAddress, unsigned char ucData)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE));
	/* Set up address and Data Registers */
	EEAR = uiAddress;
	EEDR = ucData;
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);
}char EEPROM_read(unsigned int uiAddress)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE));
	/* Set up address register */
	EEAR = uiAddress;
	/* Start eeprom read by writing EERE */
	EECR |= (1<<EERE);
	/* Return data from Data Register */
	return EEDR;
}

void mem_cpy(char* text) {
	if(saved++ > 19) {
		saved = 10;
	}
	int i = 0;
	for(; text[i] != '\0'; i++) {
		EEPROM_write(i + 50*(saved % 10), text[i]);
	}
	EEPROM_write(i + 50*(saved % 10),'\0');
	EEPROM_write(0x1FF, (saved % 10));
}

void mem_read(int index, char *result) {
	int i = 0;
	while((result[i] = EEPROM_read(index * 50 + i)) != '\0' && i < 50) {
		i++;
	}
	result[i] = '\0';
}

void gps_record() {
	static float totalWind, totalInsolation, totalTemp;
	if(recording++ <= 26) {
		totalTemp += atof(data->temp);
		totalInsolation += atof(data->insolation);
		totalWind += atof(data->wind);
	}
	else {
		recording = 0;
		char *text = malloc(50);
		printf("%f %f %f", totalTemp, totalWind, totalInsolation);
		sprintf(text, "%.6s.%.6s,%04.1f,%03.0f,%03.0f,%.1s%.10s,%.1s%.9s\r\n",
				data->date, data->time, totalWind/25.0, totalTemp/25.0,
				totalInsolation/25.0, data->coords[1].bearing,
				data->coords[1].coord, data->coords[0].bearing,
				data->coords[0].coord);
		mem_cpy(text);
		mem_read((saved % 10) - 1, text);
		printf("\nRECALLED: %s from memory at SAVE POINT %d\n", text, (saved % 10));
		free(text);
	}
}
void check_standby() {
	static int i;
	i++;
	if(PINE & (1 << PINE2)) {
		if(i < 3) {
			return;
		}
		i = 0;
		if(bit_is_set(SREG, SREG_I)) {
			cli();
			PORTE &= ~1;
			PORTD &= ~((1 << 4) | (1 << 3));
			PORTC &= ~(1 << 3);
			PORTD &= ~(1 << 2);
			UCSR0B ^= (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
			DDRB &= ~1;
		} else {
			sei();
			PORTC |= (1 << 3);
			PORTD |= (1 << 2);
			UCSR0B ^= (1<<RXEN0)|(1<<TXEN0)|(1<<RXCIE0);
			DDRB |= 1;
		}
	}
}

int main (void)
{
	//retrieve number of saves from EEPROM
	saved = EEPROM_read(0x1FF);
	if(saved > 19 || saved < 0) {
		saved = 0;
	}
	//activating interrupts for wind speed meter.
	DDRB &= ~(1<< PORTB2);
	PCICR |= (1 << PCIE0);
	PCMSK0 |= (1 << PCINT2);
	
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // Set ADC prescalar to 128 - 125KHz sample rate @ 16MHz
	ADCSRA |= (1 << ADEN);  // Enable ADC
	ADCSRA |= (1 << ADSC);  // Start A2D Conversions
	
	FILE uart_str = FDEV_SETUP_STREAM(USART0SendByte, NULL, _FDEV_SETUP_WRITE);
	stdout = &uart_str;
	
	DDRD |= (1 << 2);
	DDRC |= (1 << 3);
	PORTC |= (1 << 3);
	PORTD |= (1 << 2);
	data = malloc(sizeof(Data));
	data->date = malloc(7);
	data->time = malloc(10);
	data->temp = malloc(5);
	data->wind = malloc(5);
	data->insolation = malloc(5);
	strcpy(data->wind, "00.0");
	
	//Initialize USART0
	USART0Init();

	for(int i = 0; i < 2; i++) {
		data->coords[i].coord = malloc(11);
		data->coords[i].bearing = malloc(2);
	}
	
	init_timer0(data);
	while (1)  // Loop Forever
	{
		_delay_ms(150);
		if((PINB & (1<<PINB6)) && !recording) {
			recording = 1;
		}
		check_standby();
		sprintf(data->temp, "%03.0f",(66*GetADCValue(0))/1023.0f - 10.0f);
		sprintf(data->insolation, "%03.0f", (float)GetADCValue(1) * 1.1);
		printf("%.6s.%.6s,%s,%s,%s,%.1s%.10s,%.1s%.9s\r\n",
				data->date, data->time, data->wind, data->temp,
				data->insolation, data->coords[1].bearing,
				data->coords[1].coord, data->coords[0].bearing,
				data->coords[0].coord);
		data->displayPos = 0;
		if(recording) {
			gps_record();
		}
		if(!(data->wind[0] == '0' && data->wind[1] == '0') &&
				(get_clock_ticks() - previousTicks)
				> 8000/strtol(data->wind, NULL, 10)) {
			sprintf(data->wind, "%03.1f",
			8000.0/(get_clock_ticks() - previousTicks));
		} 
	}
}

ISR(USART0_RX_vect) {
	if(UDR0 == 'R') {
		UCSR0B ^= (1<<RXCIE0);
		receive_gps();
		UCSR0B ^= (1<<RXCIE0);
	}
}

ISR(PCINT0_vect) {
	static unsigned char popped;
	if((PINB & (1<<PINB2)) && popped) {
		if((get_clock_ticks() - previousTicks) < 10) {
			return;
		}
		sprintf(data->wind, "%03.1f", 
				(8000.0/(get_clock_ticks() - previousTicks)));
		previousTicks = get_clock_ticks();
		popped = 0;
	}
	else {
		popped = 1;
	}
}