/*
 * timer0.c
 *
 * Author: Peter Sutton
 *
 * We setup timer0 to generate an interrupt every 1ms
 * We update a global clock tick variable - whose value
 * can be retrieved using the get_clock_ticks() function.
 */

#include "timer0.h"

/* Our internal clock tick count - incremented every 
 * millisecond. Will overflow every ~49 days. */
static volatile uint32_t clock_ticks;
volatile Data* data;
unsigned char mode;

uint8_t numbers[10] = {63,6,91,79,102,109,125,7,127,111};
volatile uint8_t* ports[8] = {&PORTE, &PORTB, &PORTB, &PORTD, &PORTD, &PORTD, &PORTB, &PORTB};
volatile uint8_t* directions[8] = {&DDRE, &DDRB, &DDRB, &DDRD, &DDRD, &DDRD, &DDRB, &DDRB};
uint8_t portNum[8] = {1, 6, 7, 5, 6, 7, 0, 1};


/* Set up timer 0 to generate an interrupt every 1ms. 
 * We will divide the clock by 64 and count up to 124.
 * We will therefore get an interrupt every 64 x 125
 * clock cycles, i.e. every millisecond with an 8MHz
 * clock. 
 * The counter will be reset to 0 when it reaches it's
 * output compare value.
 */
void init_timer0(volatile Data* dataSet) {
	/* Reset clock tick count. L indicates a long (32 bit) 
	 * constant. 
	 */
	data = dataSet;
	for(unsigned char i = 0; i < 8; i++) {
		*directions[i] |= (1 << portNum[i]);
	}
	DDRD |= (1 << 3) | (1 << 4);
	DDRE |= 1;
	sei();
	clock_ticks = 0L;
	
	/* Clear the timer */
	TCNT0 = 0;

	/* Set the output compare value to be 124 */
	OCR0A = 124;
	
	/* Set the timer to clear on compare match (CTC mode)
	 * and to divide the clock by 64. This starts the timer
	 * running.
	 */
	TCCR0A = (1<<WGM01);
	TCCR0B = (1<<CS01) | (1<<CS00);

	/* Enable an interrupt on output compare match. 
	 * Note that interrupts have to be enabled globally
	 * before the interrupts will fire.
	 */
	TIMSK0 |= (1<<OCIE0A);
	
	/* Make sure the interrupt flag is cleared by writing a 
	 * 1 to it.
	 */
	TIFR0 &= (1<<OCF0A);
}

void display_digit(uint8_t num) {
	for(unsigned char i = 0; i < 8; i++) {
		if(num & (1 << i)) {
			*ports[i] |= (1 << portNum[i]);
		} else {
			*ports[i] &= ~(1 << portNum[i]);
		}
	}
}

void display(volatile char* reading) {
	if(!reading[data->displayPos] && (data->displayPos = 0));
	if(data->displayPos == 0) {
		if(reading[data->displayPos] == '0' 
				&& reading[data->displayPos + 1] != '.') {
			data->displayPos++;
			return;
		}
		PORTE |= 1;
		PORTD &= ~((1 << 3) | (1 << 4));
	} else {
		PORTD |= (1 << ((5 - data->displayPos) + (reading[data->displayPos - 1] == 46)));
		PORTE &= ~1;
	}
	display_digit(128 * (reading[data->displayPos + 1] == 46)
			+ numbers[reading[data->displayPos] - 48]);
	data->displayPos += 1 + (reading[data->displayPos + 1] == 46);
}

uint32_t get_clock_ticks(void) {
	uint32_t return_value;
	cli();
	return_value = clock_ticks;
	sei();
	return return_value;
}

/* Interrupt handler which fires when timer/counter 0 reaches 
 * the defined output compare value (every 5 milliseconds)
 */
ISR(TIMER0_COMPA_vect) {
	/* Increment our clock tick count and increment mode */
	if(!(++clock_ticks % 500) && ++mode) {
		mode %= 10;
	}
	display_digit(255);
	PORTE |= 1;
	PORTD |= (1 << 3) | (1 << 4);
	//
	if(!(clock_ticks % 5)) {
		switch(mode) {
			case 7:
			case 8:
				display(data->temp);
				break;
			case 9:
			case 6:
				PORTD &= ~((1 << 3) | (1 << 4));
				PORTE &= ~1;
				break;
			default:
				display(data->wind);
				break;
		}
	}
}
